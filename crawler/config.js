module.exports = {
  'routes' : {
    'types': '/issuetypes/',
    'issue' : '/issues'
  },
  'issuetypes' : [
    'bug', 'story'
  ],
  'API_DOMAIN' : 'http://localhost:3000'
}
