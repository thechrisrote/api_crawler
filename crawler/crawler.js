var fs = require('fs');
var config = require('./config');
var request = require('request');

function createRoute(route) {
  return config.API_DOMAIN+route
}

function logResults(state) {
  for (let key in state.issuetypes) {
    if (state.issuetypes.hasOwnProperty(key)) { // check that this isn't a protoype key
      console.log(key + " : " + state.issuetypes[key].hours);
    }
  }
}

function checkCompletion(state) {
  state.completedTasks++;
  return state.completedTasks === state.tasks.length;
}

function countEstimate(state, response, issuetype) {
  issuetype in state.issuetypes ?
    state.issuetypes[issuetype].hours += parseInt(response.estimate) :
    state.issuetypes[issuetype] = {hours: parseInt(response.estimate)}
}

function genRoutesToCrawl(issuetype, state, fn) {
  request(createRoute(`/issuetypes/${issuetype}`), function(error, response, body) {
    if (error) throw (error);
    if(!error && response.statusCode == 200) {
      let response_obj = JSON.parse(body);
      response_obj.issues.forEach(function(route) {
        let task = (function(route, issuetype) {
          return function() {
            request(createRoute(route), function (error, response, body) {
              if (error) throw(error);
              if (!error && response.statusCode == 200) {
                countEstimate(state, JSON.parse(body), issuetype);
                if (checkCompletion(state)) {
                  logResults(state);
                }
              }
            });
          }
        })(route, issuetype);
        state.tasks.push(task);
      });
      fn();
    }
  });
}

function buildFinalTree(state =  {tasks: [], issuetypes: {}, completedTasks: 0}) {
  let genRoutes = [];
  let rootRoutesProcessed = 0;
  config.issuetypes.forEach((item, index, array) => {
    genRoutesToCrawl(item, state, () => {
      rootRoutesProcessed++;
      if(rootRoutesProcessed === config.issuetypes.length) {
        for (let key in state.tasks) {
          state.tasks[key](); // run tasks async
        }
      }
    });
  });
}

// only exporting private functions for test cases
module.exports =  {
  buildFinalTree: buildFinalTree,
  _checkCompletion: checkCompletion,
  _countEstimate: countEstimate,
  _genRoutesToCrawl: genRoutesToCrawl
}
