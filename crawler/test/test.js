var assert = require('assert');
var crawler = require('../crawler');

describe('Test helper functions', function() {
  describe('checkCompletion()', function() {
    it('should return true if tasks length is one more than completed task count', function() {
      assert.equal(
        crawler._checkCompletion({ tasks : [1, 2, 3], completedTasks: 2 }), true
      );
    });
  });
  describe('countEstimate()', function() {
    it('adds to the current state hours if the issuetype exists', function() {
      let state = { tasks : [1, 2, 3], completedTasks: 2, issuetypes: {bug: {hours: 5}}};
      crawler._countEstimate(state, {'estimate': 5}, 'bug');
      assert.equal(state.issuetypes.bug.hours, 10);
    });
  });
  describe('countEstimate()', function() {
    it('creates state hours if the issuetype exists', function() {
      let state = { tasks : [1, 2, 3], completedTasks: 2, issuetypes: {} };
      crawler._countEstimate(state, {'estimate': 5}, 'bug');
      assert.equal(state.issuetypes.bug.hours, 5);
    });
  });
});
describe("Test the API", function(){
    describe("Crawl Bug Route", function(){
      let state =  {tasks: [], issuetypes: {}, completedTasks: 0};
      beforeEach(function(done){
        crawler._genRoutesToCrawl('bug', state, done);
      });
      it("should return 3 tasks to crawl", function(){
        assert.equal(state.tasks.length, 3);
      });
    });
    describe("Crawl Story Route", function(){
      let state =  {tasks: [], issuetypes: {}, completedTasks: 0};
      beforeEach(function(done){
        crawler._genRoutesToCrawl('story', state, done);
      });
      it("should return 2 tasks to crawl", function(){
        assert.equal(state.tasks.length, 2);
      });
    });
});
