## Synopsis

This project was created by Chris Rote as part of a job application to Atlassian. The following program crawls an API for some routes, then recursively crawls said routes to calculate the estimated number of hours of total issues by type.

## Motivation

This script creates a task queue, which we fire off asynchronously to crawl the API. Tasks are executed asynchronously for more efficient execution. 


## Installation

There is a test server that I created. In order to run that you must have json-server installed.

```sh
$ npm install -g json-server
$ json-server --watch db.json
```

Then navigate to the crawler directory and run

```sh
$ npm install
$ node index.js
```

And the correct output should appear.

## Tests

There are a few unit tests included in the package. You can run these if you have mocha installed. Set up the json-server as outlined above and then simply run:

```sh
$ cd test
$ mocha
```